﻿using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    public bool Automatic = false;
    public float speed = 1;

    bool Open = false;
    float InitAngle;

    private void Start()
    {
        InitAngle = transform.rotation.eulerAngles.y;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && Automatic)
            Open = true;
    }

    private void LateUpdate()
    {
        print(InitAngle);
        print(transform.rotation.eulerAngles.y);
        if ((transform.rotation.eulerAngles.y < InitAngle + 110) && Open)
            transform.Rotate(new Vector3(0, speed * Time.deltaTime, 0));
    }
}
